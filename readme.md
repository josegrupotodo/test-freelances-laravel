## Doc

composer install --prefer-dist

sudo chmod -R 777 storage/ bootstrap/ database/migrations/


configure database in file .env

php artisan migrate


## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
